// NODE.JS INTRODUCTION

//checl node version on terminal using: node --version

/*
	- use the "require" directive to load Node.js modules
	- a "module" is a software component or part of a program that contains one or more routines
	- the "http module" lets node.js transfer data using http.
	- the "http module" is set of individual files that contain code to create a "component" that helps establish data transfer between applications.
	- HTTP is a protocol that allows the fetching of resources such as html documents.
	- Clients(browser) and Servers communicate by exchanging individual messages
	- the messages sent by the client usually a web browser called "request".
	- the messages sent by the server as an answer are called "responses".
*/
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client.
// the http module has a createServer() method that accepts a function as an argument and allows for a creation of a server.
// The arguments passed in the function are request and response objects(data type) that contatins methods that allows us to receive requests from the the client and send responses back to it.
http.createServer(function(request, response){

	/* Use the writeHead() method to:
	 set a status code for the response. 200 means OK
	 Set the content-type of the response as a plain text message*/
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World');

// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// the server will be assigned to port 4000 via the "listen(4000" method where the server will listen to any requests that are sent to it, eventually communicating with our server.
}).listen(4000);

console.log('Server running at localhost:4000');

